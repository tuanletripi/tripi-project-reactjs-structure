import 'moment/locale/vi';
import React from 'react';
import { connect } from 'react-redux';
import { Route, Switch } from 'react-router';
import { ROUTES } from './configs/routes';
import { AppState } from './redux/reducers';
import Home from './modules/home/pages/Home';

function mapStateToProps(state: AppState) {
  return {
    router: state.router,
  };
}

interface Props extends ReturnType<typeof mapStateToProps> {}

const App: React.FC<Props> = props => {
  const { router } = props;

  return (
    <>
      <Switch location={router.location}>
        <Route exact path={ROUTES.home} component={Home} />
        <Route exact path="/" component={Home} />
      </Switch>
    </>
  );
};

export default connect(mapStateToProps)(App);
