import { Button, Dialog, Typography } from '@material-ui/core';
import React from 'react';
import { FormattedMessage } from 'react-intl';

interface Props {
  msgId: string;
  lang: string;
  onClose(): void;
}

const NetworkProblemDialog: React.FC<Props> = props => {
  const { msgId, onClose, lang } = props;

  return (
    <Dialog open={!!msgId} PaperProps={{ style: { padding: '24px 24px 12px' } }}>
      <Typography variant="subtitle1">
        <FormattedMessage id="notice" />
      </Typography>
      <Typography variant="body1" style={{ margin: '12px 0px 16px 0px' }}>
        <div>{msgId && <FormattedMessage id={msgId} />}</div>
      </Typography>
      <div style={{ display: 'flex', justifyContent: 'center' }}>
        <Button style={{ padding: 8 }} onClick={onClose}>
          <Typography
            variant="h6"
            color="primary"
            style={{ textTransform: 'uppercase', marginRight: 24 }}
          >
            <FormattedMessage id="retry" />
          </Typography>
        </Button>
      </div>
    </Dialog>
  );
};

export default NetworkProblemDialog;
