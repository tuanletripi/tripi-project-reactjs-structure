import styled from 'styled-components';

export const PageWrapper = styled.div`
  min-height: ${window.innerHeight}px;
  position: relative;
  display: flex;
  flex-direction: column;
`;
