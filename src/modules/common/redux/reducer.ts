import { createAction, ActionType, getType } from 'typesafe-actions';

export interface CommonState {
  networkErrorMsg: string;
  errorMessage: string;
}

export const setNetworkError = createAction('common/setNetworkError', (val: string) => ({ val }))();

export const setErrorMessage = createAction('common/setErrorMessage', (val: string) => ({ val }))();

const actions = {
  setNetworkError,
  setErrorMessage,
};

type ActionT = ActionType<typeof actions>;

export default function reducer(
  state: CommonState = {
    networkErrorMsg: '',
    errorMessage: '',
  },
  action: ActionT,
): CommonState {
  switch (action.type) {
    case getType(setErrorMessage):
      return { ...state, errorMessage: action.payload.val };
    case getType(setNetworkError):
      return { ...state, networkErrorMsg: action.payload.val };
    default:
      return state;
  }
}
