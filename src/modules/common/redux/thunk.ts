import { Action } from 'redux';
import { ThunkAction } from 'redux-thunk';
import { some } from '../../../constants';
import { AppState } from '../../../redux/reducers';
import { setNetworkError } from './reducer';

export function fetchThunk(
  url: string,
  method: 'get' | 'post' | 'delete' | 'put' = 'get',
  auth = true,
  body?: string | FormData,
  fallbackResponse?: some, // if given, will not retry at all and return this
): ThunkAction<Promise<some>, AppState, null, Action<string>> {
  return async (dispatch, getState) => {
    while (true) {
      let res;

      try {
        const headers = {
          'Content-Type': 'application/json',
          'Accept-Language': getState().intl.locale.substring(0, 2),
        };

        if (body instanceof FormData) {
          delete headers['Content-Type'];
        }

        res = await fetch(url, {
          method,
          body,
          headers,
          cache: 'no-store',
        });
      } catch (_) {}

      if (res !== undefined) {
        if (res.ok) {
          const json = await res.json();
          return json;
        }

        if (res.status === 400) {
          return await res.json();
        }

        if (res.status === 413) {
          let msg = '413';
          try {
            msg = await res.text();
          } finally {
          }
          throw new Error(msg);
        }
      }

      if (fallbackResponse) {
        return fallbackResponse;
      }

      let hasInternet = true;
      try {
        await fetch('https://tripi.vn', { mode: 'no-cors' });
      } catch (_) {
        hasInternet = false;
      }

      dispatch(setNetworkError(hasInternet ? 'serverProblem' : 'unstableNetwork'));

      do {
        await new Promise(resolve => setTimeout(resolve, 350));
      } while (getState().common.networkErrorMsg);
      continue;
    }
  };
}
