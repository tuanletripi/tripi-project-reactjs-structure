import { connectRouter, RouterState } from 'connected-react-router';
import { History } from 'history';
import { combineReducers } from 'redux';
import { PersistConfig, persistReducer } from 'redux-persist';
import storage from 'redux-persist/lib/storage/session';
import commonReducer, { CommonState } from '../modules/common/redux/reducer';
import intlReducer, { IntlState } from '../modules/intl/redux/reducer';

const persistConfig: PersistConfig<any> = {
  storage,
  key: 'root',
  transforms: [],
  blacklist: ['router', 'intl'],
};

export interface AppState {
  router: RouterState;
  intl: IntlState;
  common: CommonState;
}

export default (history: History) =>
  persistReducer(
    persistConfig,
    combineReducers({
      router: connectRouter(history),
      intl: intlReducer,
      common: commonReducer,
    }),
  );
