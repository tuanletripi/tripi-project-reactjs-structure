export type some = { [key: string]: any };

export const DEV = process.env.NODE_ENV === 'development';

export const PHONE = '0984345062';
export const TOKEN = 'token';
export const LS_LANG = 'lang';
