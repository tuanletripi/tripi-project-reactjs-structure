const proxy = require('http-proxy-middleware');

const router = {
  '/api/': 'https://target-api-url',
};

module.exports = function(app) {
  app.use(
    proxy('/api/', {
      target: 'https://target-api-url',
      changeOrigin: true,
      secure: false,
      pathRewrite: {
        '^/api/': '/',
      },
      router,
      logLevel: 'debug',
    }),
  );
};
