import { createMuiTheme, darken, fade } from '@material-ui/core/styles';
import DefaultTheme from 'react-dates/lib/theme/DefaultTheme';
import aphroditeInterface from 'react-with-styles-interface-aphrodite';
import ThemedStyleSheet from 'react-with-styles/lib/ThemedStyleSheet';
import { BLACK_TEXT, PRIMARY, SECONDARY } from './colors';

ThemedStyleSheet.registerInterface(aphroditeInterface);
ThemedStyleSheet.registerTheme({
  reactDates: {
    ...DefaultTheme.reactDates,
    color: {
      ...DefaultTheme.reactDates.color,
      selected: {
        ...DefaultTheme.reactDates.color.selected,
        backgroundColor: SECONDARY,
        backgroundColor_active: SECONDARY,
        backgroundColor_hover: SECONDARY,
        borderColor: SECONDARY,
        borderColor_active: SECONDARY,
        borderColor_hover: SECONDARY,
      },
      selectedSpan: {
        backgroundColor: fade(SECONDARY, 0.2),
        backgroundColor_active: fade(SECONDARY, 0.2),
        backgroundColor_hover: fade(SECONDARY, 0.2),
        borderColor: fade(SECONDARY, 0.2),
        borderColor_active: fade(SECONDARY, 0.2),
        borderColor_hover: fade(SECONDARY, 0.2),
      },
      hoveredSpan: {
        backgroundColor: fade(SECONDARY, 0.2),
        backgroundColor_active: fade(SECONDARY, 0.2),
        backgroundColor_hover: fade(SECONDARY, 0.2),
        borderColor: fade(SECONDARY, 0.2),
        borderColor_active: fade(SECONDARY, 0.2),
        borderColor_hover: fade(SECONDARY, 0.2),
      },
    },
  },
});

export const THEME = {
  primary: PRIMARY,
  secondary: SECONDARY,
};

export const MUI_THEME = createMuiTheme({
  palette: {
    primary: {
      light: fade(PRIMARY, 0.9),
      main: PRIMARY,
      dark: darken(PRIMARY, 0.1),
      contrastText: '#ffffff',
    },
    secondary: {
      light: fade(SECONDARY, 0.9),
      main: SECONDARY,
      dark: darken(SECONDARY, 0.1),
      contrastText: '#ffffff',
    },
    text: {
      primary: BLACK_TEXT,
      secondary: fade(BLACK_TEXT, 0.6),
    },
  },
  typography: {
    htmlFontSize: 14,
    fontSize: 14,
    subtitle1: { fontSize: '20px', fontWeight: 500 },
    subtitle2: { fontSize: '16px', fontWeight: 500, lineHeight: '26px' },
    body1: { fontSize: '16px', lineHeight: '25px' },
    body2: { fontSize: '14px', lineHeight: '23px', fontWeight: 'normal' },
    caption: { fontSize: '12px', lineHeight: '18px' },
    button: {
      fontSize: '16px',
      textTransform: 'none',
      lineHeight: '25px',
      fontWeight: 500,
    },
    h4: { fontSize: '34px', lineHeight: '40px' },
    h5: { fontSize: '24px', fontWeight: 'bold', lineHeight: '30px' },
    h6: { fontSize: '20px', lineHeight: '27px' },
  },
});
