import colors from '../scss/colors.module.scss';

export const BACKGROUND = colors.background;
let { primary } = colors;
if (!primary.startsWith('#')) {
  primary = '#00AD50';
}
let { secondary } = colors;
if (!secondary.startsWith('#')) {
  secondary = '#ff7043';
}

export const PRIMARY = primary;
export const SECONDARY = secondary;
export const BLACK_TEXT = '#212121';
export const LIGHT_GREY = '#EFF2F7';
export const GREY = '#C4C4C4';
export const BORDER_GREY = '#E6E6E6';
export const DARK_GREY = '#757575';
export const BLUE = '#30A7E0';
export const LIGHT_GREEN = '#00A13D';
export const GREEN = ' #35E578';
export const DARK_GREEN = '#10C956';
export const RED = '#F44336';
export const BROWN = '#964B00';
export const DARK_ORANGE = '#F96333';
