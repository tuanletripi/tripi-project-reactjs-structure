import { DEV } from '../constants';

function getBaseUrl() {
  return DEV ? '/api/' : 'https://target-api-url';
}

export const API_PATHS = {
  getSomeInfo: `${getBaseUrl()}/getSomeInfo`,
};
